<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      /* `app`.`groups` */
      $groups = array(
        array('name' => 'Водка','slug' => 'vodka','uri' => '/napitki/alkogol/vodka','parent' => '3','sortorder' => '2'),
        array('name' => 'Мебель','slug' => 'mebel','uri' => '/mebel','parent' => '0','sortorder' => '1'),
        array('name' => 'Алкоголь','slug' => 'alkogol','uri' => '/napitki/alkogol','parent' => '4','sortorder' => '2'),
        array('name' => 'Напитки','slug' => 'napitki','uri' => '/napitki','parent' => '0','sortorder' => '2'),
        array('name' => 'Посуда','slug' => 'posuda','uri' => '/posuda','parent' => '0','sortorder' => '3'),
        array('name' => 'Ром','slug' => 'rom','uri' => '/napitki/alkogol/rom','parent' => '3','sortorder' => '1'),
        array('name' => 'Пиво','slug' => 'pivo','uri' => '/napitki/pivo','parent' => '4','sortorder' => '3'),
        array('name' => 'Чай','slug' => 'chay','uri' => '/napitki/chay','parent' => '4','sortorder' => '1')
      );

      foreach($groups as $one)
        DB::table('groups')->insert($one);
        //
    }
}

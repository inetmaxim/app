<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      /* `app`.`items` */
      $items = array(
        array('name' => 'Погремушка','price' => '12.12','parent' => '0','sortorder' => '2'),
        array('name' => 'Стул','price' => '20.00','parent' => '2','sortorder' => '1'),
        array('name' => 'Подгузник','price' => '10.00','parent' => '0','sortorder' => '3'),
        array('name' => 'Стол','price' => '50.50','parent' => '2','sortorder' => '2'),
        array('name' => 'Терлка','price' => '7.00','parent' => '5','sortorder' => '1'),
        array('name' => 'Чашка','price' => '5.00','parent' => '5','sortorder' => '2'),
        array('name' => 'Хлебный дар','price' => '100.00','parent' => '1','sortorder' => '1'),
        array('name' => 'Немиров','price' => '90.00','parent' => '1','sortorder' => '2'),
        array('name' => 'Целсий','price' => '95.00','parent' => '1','sortorder' => '3'),
        array('name' => 'Темный ром','price' => '120.00','parent' => '6','sortorder' => '1'),
        array('name' => 'Белый ром','price' => '130.00','parent' => '6','sortorder' => '2'),
        array('name' => 'Ахмад','price' => '9.00','parent' => '8','sortorder' => '1'),
        array('name' => 'Липтон','price' => '11.00','parent' => '8','sortorder' => '2'),
        array('name' => 'Славутич','price' => '20.00','parent' => '7','sortorder' => '1'),
        array('name' => 'Оболонь','price' => '22.00','parent' => '7','sortorder' => '2'),
        array('name' => 'Stella artua','price' => '45.00','parent' => '7','sortorder' => '3')
      );

      foreach($items as $one)
        DB::table('items')->insert($one);
        //
    }
}

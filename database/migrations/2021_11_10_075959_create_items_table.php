<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('items', function (Blueprint $table) {
          $table->id();
          $table->string('name');
          $table->float('price', 8, 2);
          $table->integer('parent')->default(0);
          $table->integer('sortorder')->default(1);
          $table->timestamps();
      });

      Artisan::call('db:seed', [
          '--class' => 'ItemSeeder',
          '--force' => true 
      ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}

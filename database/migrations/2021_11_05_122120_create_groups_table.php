<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('groups', function (Blueprint $table) {
          $table->id();
          $table->string('name');
          $table->string('slug');
          $table->string('uri');
          $table->integer('parent')->default(0);
          $table->integer('sortorder')->default(1);
          $table->timestamps();
      });

      Artisan::call('db:seed', [
          '--class' => 'GroupSeeder',
          '--force' => true 
      ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}

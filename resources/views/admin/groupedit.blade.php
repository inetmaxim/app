@extends('admin.index')
@section('content')

      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="mb-3">Groups</h4>
      </div>

      <div class='col-sm-6'>
        <div class='alert alert-block alert-info'>

          <form id='form_edit'>
            <input name='cmd' value='update' type='hidden'>
            <div class="row">
              <label class="col-md-2 col-form-label">Name</label>
              <div class="col-md-10 mb-3">
                <input type="text" class="form-control" name="name" id="name" placeholder="" value="" required>
              </div>
            </div>
            <div class="row">
              <label class="col-md-2 col-form-label">Slug</label>
              <div class="col-md-10 mb-3">
                <input type="text" class="form-control" name="slug" id="slug" placeholder="" value="" required>
              </div>
            </div>
            <div>
                <a id="update" class="btn btn-primary">Save</a> <a id="new" class="btn btn-secondary">Clear</a>
            </div>
          </form>

        </div>
      </div>

    <div id="nest" class="dd"></div>

@if (!empty($json))
  <script>
  var json=<?=json_encode($json)?>;
  </script>
@endif
@endsection



@section('tools')
<script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.5.0/jquery.nestable.min.js"></script>

<style>
#nest {
  max-width:300px;
}
.sidebar-sticky {
  position: relative;
  top: 0;
  height: calc(100vh - 48px);
  padding-top: .5rem;
  overflow-x: hidden;
  overflow-y: auto;
}
</style>
<script>
$(document).ready(function() {

  var clear = function() {
    $('#name').val('');
    $('#slug').val('');
    $('#update').addClass('disabled','disabled').removeAttr('rel');
  };

  $('#new').on('click',clear);
  $('#name').on('focusout',function(){
    if ($('#slug').val()=='') $('#slug').val(translit($(this).val())); 
    if ($(this).val()!='') $('#update').removeClass('disabled');
  
  });
  $('#update').on('click',function(){
    $.ajax({
      url: '?',
      type: 'POST',
      dataType: 'json',
      data: {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        cmd:'update_group',
        name:$('#name').val(),slug:$('#slug').val(),id:$('#update').attr('rel')}
      })
      .done(function (data) {
          json = data;
          $('#nest').nestable('destroy');  
          update_tree();
          clear();          
          })
      .fail(function (request,error) {
              alert('Network error has occurred please try again!');
          });

  });
var state = null;
function update_tree() {
  $('#nest').nestable({
    json: json,
    contentCallback: function(item) {
        var content = item.content || '' ? item.content : item.title;
        content += ' <tt>[' + item.slug + ']</tt>';
        content += ' <i>(id = ' + item.id + ')</i>';
        return content;
    },
    callback: function(l,e,p){
    // l is the main container
    // e is the element that was moved
    // p is the place where element was moved.
    if (window.JSON.stringify(l.nestable('serialize'))==state) {
      $('#name').val($(e).data('title'));
      $('#slug').val($(e).data('slug'));
      $('#update').attr('rel',$(e).data('id'));
      }
    else {
      var parent=0;
      clear();
      xlist = $(e).parent();      
      if (xlist.parent().prop('tagName')=="LI") {
        parent = xlist.parent().data('id');
        }      
      list = [];
      xlist.children('li').each(function(i){
        list.push($(this).data('id'));
      });

      $.ajax({
        url: '?',
        type: 'POST',
        data: {
          "_token": $('meta[name="csrf-token"]').attr('content'),
          cmd:'update_order',parent:parent,order:list}
        })
        .done(function () {
            clear();          
            })
        .fail(function (request,error) {
                alert('Network error has occurred please try again!');
            });


      }  
    state = window.JSON.stringify($('#nest').nestable('serialize'));
    return true;
    }
  });
state = window.JSON.stringify($('#nest').nestable('serialize'));
}
  update_tree();
  clear();
});

function translit(word){
	var converter = {
		'а': 'a',    'б': 'b',    'в': 'v',    'г': 'g',    'д': 'd',
		'е': 'e',    'ё': 'e',    'ж': 'zh',   'з': 'z',    'и': 'i',
		'й': 'y',    'к': 'k',    'л': 'l',    'м': 'm',    'н': 'n',
		'о': 'o',    'п': 'p',    'р': 'r',    'с': 's',    'т': 't',
		'у': 'u',    'ф': 'f',    'х': 'h',    'ц': 'c',    'ч': 'ch',
		'ш': 'sh',   'щ': 'sch',  'ь': '',     'ы': 'y',    'ъ': '',
		'э': 'e',    'ю': 'yu',   'я': 'ya'
	};
 
	word = word.toLowerCase();
  
	var answer = '';
	for (var i = 0; i < word.length; ++i ) {
		if (converter[word[i]] == undefined){
			answer += word[i];
		} else {
			answer += converter[word[i]];
		}
	}
 
	answer = answer.replace(/[^-0-9a-z]/g, '-');
	answer = answer.replace(/[-]+/g, '-');
	answer = answer.replace(/^\-|-$/g, ''); 
	return answer;
}
</script>
@endsection

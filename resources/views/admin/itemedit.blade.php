@extends('admin.index')
@section('content')

      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="mb-3">Items</h4>
      </div>
    <div class='row'>
      <div class='col-sm-6'>
        <div class='alert alert-block alert-info'>

          <form id='form_edit'>
            <input name='cmd' value='update' type='hidden'>
            <div class="row">
              <label class="col-md-2 col-form-label">Name</label>
              <div class="col-md-10 mb-3">
                <input type="text" class="form-control" name="name" id="name" placeholder="" value="" required>
              </div>
            </div>
            <div class="row">
              <label class="col-md-2 col-form-label">Price</label>
              <div class="col-md-2 mb-3">
                <input type="number" class="form-control" name="price" id="price" placeholder="0.00" value="" required>
              </div>
            </div>
            <div class="row">
              <label class="col-md-2 col-form-label">Group</label>
              <div class="col-md-10 mb-3">
                <select class="form-control" name="group" id="group" placeholder="Group"><option value=''></option>{!! $select !!}</select>
              </div>
            </div>
            <div>
                <a id="update" class="btn btn-primary">Save</a> <a id="new" class="btn btn-secondary">Clear</a>
            </div>
          </form>

        </div>
        <div id="groups">{!! $groups !!}</div>
      </div>
      <div class='col-sm-6'>
          <table class="table table-bordered table-striped" id='itemlist'>
            <caption>List of items</caption>
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody id='bodylist'>
            </tbody>
            <tfoot>
							<tr>
								<td colspan="3" class='text-center'>No items</td>
							</tr>
            </tfoot>
          </table>

      </div>
    </div>    
@endsection

@section('tools')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


<style>
#groups ul {
  padding-left: 10px;
}

#groups li span {
  padding: 0px 10px;
  cursor: pointer;
}
#groups li span.selected {
  background-color: #007bff;
  color: #fff;
}
#groups ul li {
  list-style-type: none;
  text-decoration: underline;
  color: blue;
}
.sidebar-sticky {
  position: relative;
  top: 0;
  height: calc(100vh - 48px);
  padding-top: .5rem;
  overflow-x: hidden;
  overflow-y: auto;
}
#itemlist caption {
  caption-side: top;
}

</style>
<script>
function get_items() {
    $.ajax({
      url: '?',
      type: 'POST',
      dataType: 'json',
      data: {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        cmd:'list_items',
        group:$('#group').val()}
      })
      .done(function (data) {
        $('#itemlist tbody').html('');
        if (data.lenght==0) {
          $('#itemlist tfoot').removeClass('d-none');
          }  
        else {
          $.each(data,function(i,o){
            $('#itemlist tbody').append(
              $('<tr/>',{rel:o.id}).append(
                $('<td/>',{text:o.id}),
                $('<td/>',{text:o.name}),
                $('<td/>',{text:o.price})
                )
              );
            $('#itemlist tfoot').addClass('d-none');
          });        

          $('#bodylist').sortable({
            helper: function(e, ui) {
              ui.children().each(function() {
                $(this).width($(this).width());
              });
            return ui;
            },
            stop: function(e,ui) {
              var list = [];
              $('#itemlist tbody tr').each(function(i){
                list.push($(this).attr('rel'));
              });

              $.ajax({
                url: '?',
                type: 'POST',
                data: {
                  "_token": $('meta[name="csrf-token"]').attr('content'),
                  cmd:'update_order',group:$('#group').val(),order:list}
                })
                .done(function () {
                  clear();
                })
                .fail(function (request,error) {
                  alert('Network error has occurred please try again!');
                });
            }
          });
          $('#bodylist tr').on('click',function(){
            $('#update').attr('rel',$(this).attr('rel')).removeClass('disabled');
            $('#name').val($(this).children('td:nth-child(2)').text());
            $('#price').val($(this).children('td:nth-child(3)').text());
          });
        }
      })
      .fail(function (request,error) {
        alert('Network error has occurred please try again!');
      });
}  

$(document).ready(function() {

  var clear = function() {
    $('#name').val('');
    $('#price').val('');
    $('#update').addClass('disabled').removeAttr('rel');
  };

  $('#new').on('click',function(){
    $('#group').val('');    
    $('#groups li span').removeClass('selected');
    clear();
  });
  $('#name').on('focusout',function(){
    if ($(this).val()!='') $('#update').removeClass('disabled');
  });

  $('#update').on('click',function(){
    $.ajax({
      url: '?',
      type: 'POST',
      dataType: 'json',
      data: {
        "_token": $('meta[name="csrf-token"]').attr('content'),
        cmd:'update_item',
        name:$('#name').val(),price:$('#price').val(),group:$('#group').val(),id:$('#update').attr('rel')}
      })
      .done(function (data) {
          get_items();
          clear();
          })
      .fail(function (request,error) {
              alert('Network error has occurred please try again!');
          });

  });


$('#groups li span').on('click',function(){
  $('#groups li span').removeClass('selected');
  $(this).addClass('selected');
  $('#group').val($(this).parent().attr('rel'));
  get_items();
  });
  clear();
  get_items();
});
</script>
@endsection

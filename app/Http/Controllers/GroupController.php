<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Group;
use App\Models\Item;


class GroupController extends Controller
{
    public function edit(Request $req) {

      if (!empty($req->input('cmd'))) {
        $_f = $req->input('cmd');
        if (method_exists($this,$_f)) {
          $this->$_f($req);
        }
        else
          dd($req);
      }

      $json = $this->get_tree();
      return view('admin.groupedit',compact('json'));
    }

    public function show(Request $req, $path=null) {

//       dump($req);
//       dd($path);
      $bc = [];
      $path=Str::start($path, '/');
      if ($path!='/') {
        $group = Group::where('uri',$path)->first();
        $group_id = $group->id;
        $id = $group->parent;
        while($id>0) {
          $g = Group::find($id);
          array_unshift($bc,$g);
          $id=$g->parent;
        }
      }
      else {
        $group = null;
        $group_id = 0;
      }

      $groups = Group::where('parent', $group_id)
               ->orderBy('sortorder')
               ->get();

      // $items = Item::where('parent', $group_id)
      //          ->orderBy('sortorder')
      //          ->get();

      $items = DB::select("SELECT * FROM items WHERE parent in (SELECT id FROM groups WHERE uri like '{$path}%')");




      return view('catalog',compact('group','groups','items','bc'));
    }
    private function update_order($req) {
      $txt="";
      if (!empty($req->input('order'))) {
        $prefix='';
        if (!empty($req->input('parent'))) {
          $parentgroup = Group::find($req->input('parent'));
          $prefix = $parentgroup->uri;
        }
        $prefix.="/";
        foreach($req->input('order') as $i=>$id) {
          $group = Group::find($id);
          $group->sortorder=$i+1;
          $group->parent = $req->input('parent');
          if ($group->uri != $prefix.$group->slug) {
            $q="UPDATE groups SET uri = REPLACE(uri, '{$group->uri}','{$prefix}{$group->slug}') WHERE uri like '{$group->uri}/%'";
            $txt.=$q."=".DB::update($q);
            $txt." <br>\n";
            }
          $group->uri = $prefix.$group->slug;
          $group->save();
        }
      print $txt;
      }
      exit;
    }
    private function update_group($req) {
      if (!empty($req->input('id'))) {
        $group = Group::find($req->input('id'));
        $group->name = $req->input('name');
        $group->slug = $req->input('slug');
        $group->save();
      }
      else {
        $group = new Group();
        $group->name = $req->input('name');
        $group->slug = $slug = Str::slug($req->input('slug'),'-');
        if (empty($req->input('path'))) {
          $group->uri = Str::start($group->slug, '/');
          $group->parent = 0;
          }
        $res = DB::select("select max(sortorder) as m from groups where parent=?",[$group->parent]);
        $group->sortorder = $res[0]->m + 1;
        $group->save();
      }

      $json = $this->get_tree();
      print json_encode($json);
      exit;
      }

    public function get_tree() {

      $groups = DB::table('groups')->get();
      $sorted = $groups->sortBy('sortorder');
      $sorted = $sorted->toArray();
      $json=[];

      while(!empty($sorted)) {
        foreach($sorted as $i=>$one) {
          $ret = self::add_item($json,$one);
          if ($ret) {
            unset($sorted[$i]);
            break;
          }
        }
      }

      return $json;
    }

    private function add_item(&$hash,$one) {
      if ($one->parent==0) {
        $hash[]=[
          "id"=>$one->id,
          "title"=>$one->name,
          "slug"=>$one->slug,
          ];
        return true;
        }
      else {
        foreach($hash as $i=>$a) {
          if ($a['id']==$one->parent) {
            if (!isset($a['children'])) $hash[$i]['children']=[];
            $hash[$i]['children'][]=[
              "id"=>$one->id,
              "title"=>$one->name,
              "slug"=>$one->slug,
              ];
            return true;
          }
          if (!empty($a['children'])) {
            $ret = self::add_item($hash[$i]['children'],$one);
            if ($ret) return $ret;
            }

          }


        }
    return false;
    }

}

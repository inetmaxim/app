<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\GroupController;
use App\Models\Item;

class ItemController extends Controller
{
    //
    public function edit(Request $req) {

      if (!empty($req->input('cmd'))) {
        $_f = $req->input('cmd');
        if (method_exists($this,$_f)) {
          $this->$_f($req);  
        }
        else
          dd($req);
      }
    
      $json = GroupController::get_tree();
      
      $groups = $this->make_ul($json);
      $select = $this->make_select($json);
      
      return view('admin.itemedit',compact('groups','select'));
    }

    private function update_item($req) {
      if (!empty($req->input('id'))) {
        $item = Item::find($req->input('id'));
        $item->name = $req->input('name');
        $item->price = floatval($req->input('price'));
        $item->parent = intval($req->input('group'));
        $item->save();
      }
      else {
        $item = new Item();
        $item->name = $req->input('name');
        $item->price = floatval($req->input('price'));
        $item->parent = intval($req->input('group'));
        $res = DB::select("select max(sortorder) as m from items where parent=?",[$item->parent]);
        $item->sortorder = $res[0]->m + 1;
        $item->save();
      }      
      exit;
    } 
    private function update_order($req) {
      $txt="";
      if (!empty($req->input('order'))) {
        foreach($req->input('order') as $i=>$id) {
          $item = Item::find($id);
          $item->sortorder=$i+1;
          $item->save();
        }
      print $txt;
      }
      exit;
    }

    private function list_items($req) {
      $items = Item::where('parent', intval($req->input('group')))
               ->orderBy('sortorder')
               ->get();
    
      print json_encode($items);
      exit;
    }



    private function make_ul($hash) {
      if (empty($hash)) return;
      $str="<ul>\n";
      foreach($hash as $one) {
        $str.="<li rel='{$one['id']}'><span>{$one['title']}</span>";
        if (!empty($one['children']))
          $str.=$this->make_ul($one['children']);
        $str.="</li>\n";  
        }
      $str.="</ul>\n";      
      return $str;
    }

    private function make_select($hash,$prefix="") {
      if (empty($hash)) return;
      $str="";
      foreach($hash as $one) {
        $str.="<option value='{$one['id']}'>{$prefix}{$one['title']}</option>\n";
        if (!empty($one['children']))
          $str.=$this->make_select($one['children'],"--".$prefix);
        }
      return $str;
    }


}

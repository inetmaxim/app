<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-type/4%20PNG/3%20RGB/1%20Full%20Color/laravel-logotype-rgb-red.png"></a></p>

# Laravel test catalog

## Task project

Нужно реализовать дерево категорий с неограниченным уровнем вложенности.
Пример:
Категория-1
	Категория-1-1
	Категория-1-2
		Категория-1-2-1
Категория-2
и т.д.
1. Возможность добавление/редактирование Категории
2. Возможность сортировки дерева и вложенности.
Можно реализовывать удобным вам способом:
	1) или одим из существующих плагинов drug-and-drop
	2) или связанными <select> на формах добавления/редактирования категории.

3. У категорий есть товары (минимум полей, название и цена достаточно).
При переходе на категорию отобразить все товары этой категории и всех ее подкатегорий.
4. Реализовать добавление и редактирование товара с привязкой к категории.

## Installation

Выполняем команды:

```
git clone https://gitlab.com/inetmaxim/app.git
cd app
composer update
cp .env.example .env
php artisan key:generate
```
Отредактировать фрагмент файла .env с параметрами соединения к серверу БД
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=app
DB_USERNAME=root
DB_PASSWORD=
```

Далее выполняем
```
php artisan migrate
php artisan serve
```
и открываем http://127.0.0.1:8000/

## About version

Laravel Framework 8.73.2

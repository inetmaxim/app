<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\ItemController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {
  Route::match(['get', 'post'],'/groups', [GroupController::class, 'edit'])->name('groups');
  Route::match(['get', 'post'],'/items', [ItemController::class, 'edit'])->name('items');
  Route::redirect('/', '/admin/groups');
  });

Route::get('/test/{path}', function ($path) {
    return "<tt>#$path!</tt>";
});

Route::get('/catalog/{x?}' ,[GroupController::class, 'show'])->where('x', '.*');

Route::redirect('/', '/catalog');


